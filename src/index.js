import React from "react";
import ReactDOM from "react-dom";
import "./styles/index.css";
// import { LazyLoadCargaMultiple } from "./LazyLoad_Basic_CargaMultiple/LazyLoadCargaMultiple";
// import { LazyLoadCargaUnica } from "./LazyLoad_Basic_CargaUnica/LazyLoadCargaUnica";
import { LazyLoadCustomHook } from "./LazyLoad_customHook/LazyLoadCustomHook";

ReactDOM.render(
  <React.StrictMode>
    {/* <LazyLoadCargaMultiple /> */}
    {/* <LazyLoadCargaUnica /> */}
    <LazyLoadCustomHook />
  </React.StrictMode>,
  document.getElementById("root")
);
