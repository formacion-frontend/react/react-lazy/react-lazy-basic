import React from "react";
import { LazyBasicComponent } from "./components/LazyLoad_nComponents/BasicComponent"; //Componente que usa el useLazyLoad para multiples referencias en un componente.
// import { LazyBasicComponent } from "./components/LazyLoad_oneComponent/BasicComponent";       //Componente que usa el useLazyLoad para una referencia por componente.

export const LazyLoadCustomHook = () => {
  return (
    <>
      <h1>Uso funcional Lazy Load</h1>
      <hr />
      <div className="separacion">
        <p></p>
      </div>
      <div className="componente">
        <LazyBasicComponent></LazyBasicComponent>
      </div>
      <div className="cambioCara">
        <b>CUANDO PUEDAS LEER ESTO LA CARA HABRA CAMBIADO A FELIZ</b>
      </div>
    </>
  );
};
