import React, { useRef } from "react";
import { CaraFeliz } from "../CaraFeliz";
import { CaraTriste } from "../CaraTriste";
import "antd/dist/antd.css";
import { useLazyLoadNComponents } from "../../hooks/useLazyLoadNComponents";

const BasicComponent = ({ mostrar }) => {
  return <>{mostrar ? <CaraFeliz></CaraFeliz> : <CaraTriste></CaraTriste>}</>;
};

export const LazyBasicComponent = () => {
  const componenteCaras = useRef();

  const isNearScreen = useLazyLoadNComponents({
    refElement: componenteCaras,
    distance: -420,
  });

  return (
    <div ref={componenteCaras}>
      {isNearScreen ? (
        <BasicComponent mostrar={isNearScreen} />
      ) : (
        <BasicComponent mostrar={isNearScreen} />
      )}
    </div>
  );
};
