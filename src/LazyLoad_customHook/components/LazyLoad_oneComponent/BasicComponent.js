import React from "react";
import { CaraFeliz } from "../CaraFeliz";
import { CaraTriste } from "../CaraTriste";
import "antd/dist/antd.css";
import { useLazyLoadOneComponent } from "../../hooks/useLazyLoadOneComponent";

const BasicComponent = ({ mostrar }) => {
  return <>{mostrar ? <CaraFeliz></CaraFeliz> : <CaraTriste></CaraTriste>}</>;
};

export const LazyBasicComponent = () => {
  const { isNearScreen, fromRef } = useLazyLoadOneComponent({
    distance: -420,
  });

  return (
    <div ref={fromRef}>
      {isNearScreen ? (
        <BasicComponent mostrar={isNearScreen} />
      ) : (
        <BasicComponent mostrar={isNearScreen} />
      )}
    </div>
  );
};
