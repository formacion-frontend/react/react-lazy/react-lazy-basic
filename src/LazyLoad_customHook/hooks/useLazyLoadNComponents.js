import { useEffect, useState } from "react";
//PASAMOS POR PARAMETRO EL NOMBRE DEL ELEMENTO QUE VAMOS A REFERENCIAR, CON ESTO PERMITIMOS USAR ESTE HOOK EN UN MISMO COMPONENTE VARIAS VECES, DEVUELVE SOLO UN TRUE O UN FALSE EN FUNCION DE LA DISTANCIA QUE HAYA AL COMPONENTE.
export const useLazyLoadNComponents = ({ refElement, distance = "100" }) => {
  const [isNearScreen, setIsNearScreen] = useState(false);

  useEffect(() => {
    const onChange = (entries) => {
      const elemento1 = entries[0];
      if (elemento1.isIntersecting) {
        setIsNearScreen(true);
        observer.disconnect(); //Se deja de observar el elemento referenciado.
      }
    };
    const observer = new IntersectionObserver(onChange, {
      rootMargin: `${distance}px`, //mide la distancia que hay al elemento
    });
    observer.observe(refElement.current); //Se observa constantemente el elemento referenciado.
  });

  return isNearScreen;
};
