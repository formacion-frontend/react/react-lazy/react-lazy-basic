import React from "react";
import { LazyBasicComponent } from "./components/BasicComponent";

export const LazyLoadCargaMultiple = () => {
  return (
    <>
      <h1>Uso funcional Lazy Load</h1>
      <hr />
      <div className="separacion">
        <p></p>
      </div>
      <div className="componente">
        <LazyBasicComponent></LazyBasicComponent>
      </div>
      <div className="cambioCara">
        <b>CUANDO PUEDAS LEER ESTO LA CARA HABRA CAMBIADO A FELIZ</b>
      </div>
    </>
  );
};
