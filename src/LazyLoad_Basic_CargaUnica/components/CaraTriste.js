import React from "react";
import { Result } from "antd";
import { FrownOutlined } from "@ant-design/icons";

export const CaraTriste = () => {
  return (
    <>
      <Result
        icon={<FrownOutlined />}
        title="El Lazy component sigue sin estar cargado!!"
      />
    </>
  );
};
