import React, { useEffect, useRef, useState } from "react";
import { CaraFeliz } from "./CaraFeliz";
import { CaraTriste } from "./CaraTriste";
import "antd/dist/antd.css";

const BasicComponent = ({ mostrar }) => {
  return <>{mostrar ? <CaraFeliz></CaraFeliz> : <CaraTriste></CaraTriste>}</>;
};

//-------------------------------------------------------------------------------   LAZY LOAD BASADO EN BasicComponent
export const LazyBasicComponent = () => {
  const [show, setShow] = useState(false);
  const elementoDivRef = useRef();

  useEffect(() => {
    const onChange = (entries) => {
      const elemento1 = entries[0];
      if (elemento1.isIntersecting) {
        setShow(true);
        observer.disconnect();
      }
    };

    const observer = new IntersectionObserver(onChange, {
      rootMargin: "-440px", //Especificamos la distancia a la que se carga el lazy en este caso hemos puesto -450 para que se cargue una vez lo hayamos sobrepasado para poder ver la diferencia, lo correcto seria poner un valor en positivo lo que
      // -------------------permite que se cargue X pixeles antes de que el usuario llegue a el , creando una experiencia de nevgacion mas fluida y amigable.
    });

    observer.observe(elementoDivRef.current);
  });

  return (
    //------------------------------------------ Estamos escuchando este elemento div viendo a que distancia esta del viewport constantemente , luego realizamos un condicional para decidir que elemento mostrar.
    <div ref={elementoDivRef}>
      {show ? (
        <BasicComponent mostrar={show} /> //Se muestra este componente si show es true
      ) : (
        <BasicComponent mostrar={show} /> //Se muestra este componente si show es false
      )}
    </div>
  );
};
